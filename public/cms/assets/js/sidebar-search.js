$(document).ready(function() {
    // Function to filter sections based on search input
    function filterSections() {
        var searchText = $('#sectionSearch').val().toLowerCase();
        var anySectionsVisible = false; // Flag to track if any sections are visible
        $('#menuSections .menu-header').each(function() {
            var $section = $(this);
            var sectionVisible = false; // Flag to track if the section is visible
            $section.nextUntil('.menu-header').each(function() {
                var $menuItem = $(this);
                var sectionText = $menuItem.text().toLowerCase();
                if (sectionText.includes(searchText)) {
                    $menuItem.show();
                    sectionVisible = true;
                    anySectionsVisible = true;
                } else {
                    $menuItem.hide();
                }
            });
            // Show/hide the menu header based on the section visibility
            if (sectionVisible) {
                $section.show();
            } else {
                $section.hide();
            }
        });
        // Hide the entire menu if no sections are visible
        if (!anySectionsVisible) {
            $('#menuSections').hide();
        } else {
            $('#menuSections').show();
        }
    }

    // Trigger filterSections function on input change
    $('#sectionSearch').on('input', filterSections);

    // Trigger filterSections function on blur (when the input loses focus)
    $('#sectionSearch').on('blur', filterSections);
});
