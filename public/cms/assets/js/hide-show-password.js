document.addEventListener('DOMContentLoaded', function() {
    const passwordInput = document.getElementById('password');
    const togglePassword = document.getElementById('toggle-password');

    if (passwordInput && togglePassword) {
        togglePassword.addEventListener('click', function() {
            if (passwordInput.type === 'password') {
                passwordInput.type = 'text';
                togglePassword.innerHTML = '<i class="bx bx-show"></i>';
            } else {
                passwordInput.type = 'password';
                togglePassword.innerHTML = '<i class="bx bx-hide"></i>';
            }
        });
    }
});
