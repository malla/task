<nav class="nav-pagination" aria-label="Page navigation">

    <div class="label">
        <span>Showing {{ $items->firstItem() ?: 0 }} to {{ $items->lastItem() ?: 0 }}
            of total {{ $items->total() }} entries</span>
    </div>
    <ul class="pagination">

        <li class="">
            <div class="btn-group">
                <button class="btn btn-outline-primary dropdown-toggle" type="button" data-bs-toggle="dropdown"
                    aria-expanded="false">{{ currentLimit() }}</button>
                <ul class="dropdown-menu" style="min-width: auto;">
                    @foreach (limits() as $limit)
                        <li><a class="dropdown-item {{ $limit['active'] ? 'active' : '' }}"
                                href="{{ $limit['url'] }}">{{ $limit['label'] }}</a></li>
                    @endforeach
                </ul>
            </div>
        </li>
        {!! $items->links() !!}
    </ul>
</nav>
