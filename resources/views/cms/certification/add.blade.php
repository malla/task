@extends('cms.layouts.app')
@section('title', 'Add Certification')

@section('content')
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Certification/</span> Add</h4>

    <!-- Basic Layout -->
    <div class="row">
        <div class="col-xl">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h5 class="mb-0">Add certification</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('cms.certification.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="mb-3 col-lg-12">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="active" name="active" {{ old('active') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="active">Active</label>
                                </div>
                                @error('active')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3 col-lg-12">
                                <label class="form-label" for="name">Name</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Ali" value="{{ old('name') }}" required>
                                @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        <div class="mb-3 col-lg-12 col-md-6 col-sm-6 text-end">
                            <a href="{{ route('cms.certification.index') }}" type="button" class="btn btn-secondary">Back</a>
                            <button id="disbaled_button" type="submit"  class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
