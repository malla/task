@extends('cms.layouts.app')
@section('title', 'certification')

@section('content')
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Certification/</span> List</h4>

    <!-- Basic Layout -->

    <div class="row" style="margin: 0px">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif
        <div class="card">

                <form action="" method="get">
                    <div class="row my-3">
                        <div class="col-lg-3 col-sm-6  col-xs-6 mt-1">
                            <input type="text" class="form-control" name="search" value="{{ request()->get('search') }}" placeholder="Search">
                        </div>
                        <div class="col-lg-3 col-sm-6  col-xs-6 mt-1">
                            <select class="form-select select2" name="status">
                                <option value="">Filter by Status</option>
                                <option value="active" {{ request()->get('status') == 'active' ? 'selected' : '' }}>Active</option>
                                <option value="inactive" {{ request()->get('status') == 'inactive' ? 'selected' : '' }}>Inactive</option>
                            </select>
                        </div>

                        <div class="col-lg-5  col-9 col-md-4 mt-1">
                            <button type="submit" class="btn btn-primary">Filter</button>
                            <a href="{{ route('cms.certification.index') }}" class="btn btn-secondary">Clear</a>
                            <a href="{{ route('cms.certification.exportExcel',$_GET) }}" class="btn btn-success">Excel</a>
                            <a href="{{ route('cms.certification.exportPdf',$_GET) }}" class="btn btn-danger">Pdf</a>
                        </div>

                        <div class="col-lg-1  col-3 col-md-2 mt-1">
                            <a href="{{ route('cms.certification.add') }}" class="btn  btn-primary "> Add </a>
                        </div>

                    </div>
                </form>
                <div class="table-responsive text-nowrap mt-2">
                    <table class="table ">
                        <thead class="table-light">
                            <tr>
                                <th>SL</th>
                                <th> Name</th>
                                <th>User Counts</th>
                                <th>Status</th>
                                {{-- <th>switch</th> --}}
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody class="table-border-bottom-0">
                            @forelse ($certifications as $key=> $certification)
                            <tr>
                                <td>{{$certifications->firstitem()+$key}}</td>
                                <td>{{ $certification->name }}</td>
                                <td>{{ $certification->certifications_count }}</td>



                                {{-- <td>
                                    <span class="badge bg-label-{{ $certification->status === 'active' ? 'success' : 'danger' }}">
                                {{ $certification->status }}
                                </span>
                                </td> --}}
                                <td class="">
                                    <div class="form-check form-switch ">
                                        <input class="form-check-input status-switch" type="checkbox" data-certification-id="{{ $certification->id }}" {{ $certification->status === 'active' ? 'checked' : '' }}>
                                    </div>
                                </td>
                                <td>

                                    <a href="{{ route('cms.certification.edit', $certification->id) }}"><span style="font-size: 24px; color: grey;">&#9998;</span></a>
                                    <a href="{{ route('cms.certification.destroy', $certification->id) }}" id="delete-certification-{{ $certification->id }}">
                                        <span style="font-size: 24px; color: red;">&#128465;</span>
                                    </a>

                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="8" class="empty">No Result Found</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                </div>
                @include('cms.components.pagination', ['items' => $certifications])

        </div>
    </div>

</div>
<!-- / Content -->

@endsection
@push('scripts')

<script>
    $(document).ready(function() {

        // Listen for clicks on delete links with IDs starting with "delete-certification-"
        $('a[id^="delete-certification-"]').on('click', function(e) {
            e.preventDefault();
            var deleteUrl = $(this).attr('href');

            // Show a Toastr confirmation message
            toastr.options = {
                closeButton: true
                , progressBar: true
                , positionClass: 'toast-top-center'
                , timeOut: 2500, // Adjust the duration as needed
            };

            toastr.warning('Are you sure you want to delete this certification?', 'Confirmation', {
                closeButton: true
                , timeOut: 2500
                , extendedTimeOut: 2500
                , tapToDismiss: false
                , progressBar: false
                , closeHtml: '<button><i class="fa fa-times"></i></button>'
                , preventDuplicates: true
                , onclick: function() {
                    // If the user clicks "Yes," proceed to the delete action
                    window.location.href = deleteUrl;
                }
            });
        });
        $('.status-switch').on('change', function() {
            const certificationId = $(this).data('certification-id');
            const newStatus = this.checked ? 'active' : 'inactive';

            $.ajax({
                type: 'POST'
                , url: '{{ route("cms.certification.status") }}'
                , data: {
                    '_token': '{{ csrf_token() }}'
                    , 'certification_id': certificationId
                    , 'status': newStatus
                }
                , success: function(response) {
                    // Check if the status change was successful

                    if (response.success) {
                        toastr.success('Status changed successfully'); // Display a success message
                    } else {
                        toastr.error('Status change failed'); // Display an error message
                    }

                }
                , error: function(error) {
                    toastr.error('An error occurred'); // Display an error message
                    console.error('Error:', error);
                }
            });
        });

    });

</script>
@endpush
