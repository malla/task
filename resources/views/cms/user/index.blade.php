@extends('cms.layouts.app')
@section('title', 'user')

@section('content')
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">user/</span> List</h4>

    <!-- Basic Layout -->

    <div class="row" style="margin: 0px">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="card">

            <form action="" method="get">
                <div class="row my-3">
                    <div class="col-lg-4 col-sm-6  col-xs-6 mt-1">
                        <input type="text" class="form-control" name="search" value="{{ request()->get('search') }}"
                            placeholder="Search">
                    </div>
                    <div class="col-lg-4 col-sm-6  col-xs-6 mt-1">
                        <select class="form-select select2" name="status">
                            <option value="">Filter by Status</option>
                            <option value="active" {{ request()->get('status') == 'active' ? 'selected' : '' }}>Active
                            </option>
                            <option value="inactive" {{ request()->get('status') == 'inactive' ? 'selected' : ''
                                }}>Inactive</option>
                        </select>
                    </div>
                    <div class="col-lg-3  col-9 col-md-4 mt-1">
                        <button type="submit" class="btn btn-primary">Filter</button>
                        <a href="{{ route('cms.user.index') }}" class="btn btn-secondary">Clear</a>
                    </div>


                </div>
            </form>
            <div class="table-responsive text-nowrap mt-2">
                <table class="table">
                    <thead class="table-light">
                        <tr>
                            <th>SL</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Blood Type</th>
                            <th>Certifications</th>
                            <th>Status</th>

                        </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                        @forelse ($users as $key=> $user)
                        <tr>
                            <td>{{$users->firstitem()+$key}}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->gender }}</td>
                            <td>{{ $user->blood_type }}</td>
                            <td>
                                @if ($user->certifications->isNotEmpty())
                                @foreach ($user->certifications as $certificate)
                                {{ $certificate->certification->name }}
                                @unless ($loop->last),@endunless
                                @endforeach
                                @else
                                N/A
                                @endif
                            </td>


                            <td class="">
                                <div class="form-check form-switch ">
                                    <input class="form-check-input status-switch" type="checkbox"
                                        data-user-id="{{ $user->id }}" {{ $user->status === 'active' ? 'checked' : ''
                                    }}>
                                </div>
                            </td>
                           
                        </tr>
                        @empty
                        <tr>
                            <td colspan="7" class="empty">No Result Found</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            @include('cms.components.pagination', ['items' => $users])
        </div>
    </div>

</div>
<!-- / Content -->

@endsection
@push('scripts')

<script>
    $(document).ready(function() {

        // Listen for clicks on delete links with IDs starting with "delete-user-"
        $('a[id^="delete-user-"]').on('click', function(e) {
            e.preventDefault();
            var deleteUrl = $(this).attr('href');

            // Show a Toastr confirmation message
            toastr.options = {
                closeButton: true
                , progressBar: true
                , positionClass: 'toast-top-center'
                , timeOut: 2500, // Adjust the duration as needed
            };

            toastr.warning('Are you sure you want to delete this user?', 'Confirmation', {
                closeButton: true
                , timeOut: 2500
                , extendedTimeOut: 2500
                , tapToDismiss: false
                , progressBar: false
                , closeHtml: '<button><i class="fa fa-times"></i></button>'
                , preventDuplicates: true
                , onclick: function() {
                    // If the user clicks "Yes," proceed to the delete action
                    window.location.href = deleteUrl;
                }
            });
        });

        $('.status-switch').on('change', function() {
            const userId = $(this).data('user-id');
            const newStatus = this.checked ? 'active' : 'inactive';

            $.ajax({
                type: 'POST'
                , url: '{{ route("cms.user.status") }}'
                , data: {
                    '_token': '{{ csrf_token() }}'
                    , 'user_id': userId
                    , 'status': newStatus
                }
                , success: function(response) {
                    // Check if the status change was successful

                    if (response.success) {
                        toastr.success('Status changed successfully'); // Display a success message
                    } else {
                        toastr.error('Status change failed'); // Display an error message
                    }

                }
                , error: function(error) {
                    toastr.error('An error occurred'); // Display an error message
                    console.error('Error:', error);
                }
            });
        });
    });

</script>
@endpush
