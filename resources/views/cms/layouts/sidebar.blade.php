<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="{{ route('cms.dashboard') }}" class="app-brand-link">
            <span class="app-brand-logo demo">
            <img src="{{ asset('/cms/assets/img/logo.png') }}" alt="">
            </span>
            {{-- <span class="app-brand-text demo menu-text fw-bolder ms-2">Sneat</span> --}}
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>
    <div class="search-input mx-3">
        {{-- <input type="text" class="form-control" placeholder="Search..." id="sectionSearch"> --}}

    </div>
    <ul class="menu-inner py-1" id="menuSections">

        <!-- Dashboard -->
        <li class="menu-item  {{ Request::is('cms/dashboard*') ? 'active' : '' }}" id="parent">
            <a href="{{ route('cms.dashboard') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>

        <!-- End Dashboard -->


        <!-- Users -->
        <li class="menu-header small text-uppercase">
            <span class="menu-header-text">User Management</span>
        </li>


        <li class="menu-item {{ Request::is('cms/admin*') || Request::is('cms/admin/add') ? 'active' : '' }}" id="parent">
            <a href="{{ route('cms.admin.index') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-user"></i>
                <div data-i18n="Account">Admin</div>
            </a>
        </li>

        <li class="menu-item {{ Request::is('cms/user*')  ? 'active' : '' }}" id="parent">
            <a href="{{ route('cms.user.index') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-user"></i>
                <div data-i18n="Account">user</div>
            </a>
        </li>



       <li class="menu-header small text-uppercase">
            <span class="menu-header-text">Certification Mangment</span>
        </li>
        <li class="menu-item  {{ Request::is('cms/certification*') ? 'active' : '' }}" id="parent">
            <a href="{{ route('cms.certification.index') }}" class="menu-link">
                <i class="menu-form-icon tf-icons bx bx-file" style="margin-right: 10px"></i>
                <div data-i18n="Analytics">Certification</div>
            </a>
        </li>

       



    </ul>
</aside>

