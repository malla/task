<script src="{{ asset('../cms/assets/vendor/js/helpers.js') }}"></script>


<script src="{{ asset('../cms/assets/js/config.js') }}"></script>

<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="{{ asset('../cms/assets/vendor/libs/jquery/jquery.js')}}"></script>
<script src="{{ asset('../cms/assets/vendor/libs/popper/popper.js')}}"></script>
<script src="{{ asset('../cms/assets/vendor/js/bootstrap.js')}}"></script>
<script src="{{ asset('../cms/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>

<script src="{{ asset('../cms/assets/vendor/js/menu.js') }}"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="{{ asset('../cms/assets/vendor/libs/apex-charts/apexcharts.js')}}"></script>

<!-- Main JS -->
<script src="{{ asset('../cms/assets/js/main.js') }}"></script>
<script src="{{ asset('../cms/assets/js/console.js') }}"></script>
<script src="{{ asset('../cms/assets/js/sidebar-search.js') }}"></script>
<script src="{{ asset('../cms/assets/js/hide-show-password.js') }}"></script>
<script src="{{ asset('../cms/assets/js/disbaled-button.js') }}"></script>




<!-- Page JS -->
<script src="{{ asset('../cms/assets/js/dashboards-analytics.js')}}"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="{{asset('https://buttons.github.io/buttons.js')}}"></script>

<script src="{{ asset('https://code.jquery.com/jquery-3.6.0.min.js')}}"></script>
<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js')}}"></script>

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js')}}"></script>

<script src="{{ asset('https://cdn.ckeditor.com/4.16.2/standard/ckeditor.js')}}"></script>

<script src="{{ asset('https://code.jquery.com/ui/1.12.1/jquery-ui.js')}}"></script>

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-rc.0/js/select2.min.js')}}"></script>

<script src="{{ asset('../cms/assets/js/fancybox.js') }}"></script>
<script src="{{ asset('../cms/assets/js/select2.js') }}"></script>


<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.min.js')}}"></script>

<script src="{{ asset('../cms/assets/js/selectize.js') }}"></script>


@stack('scripts')
