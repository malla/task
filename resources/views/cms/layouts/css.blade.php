    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset('../cms/assets/img/favicon/favicon.ico') }}" />

    <!-- Fonts -->
    <link rel="preconnect" href="{{ asset('https://fonts.googleapis.com')}}" />
    <link rel="preconnect" href="{{ asset('https://fonts.gstatic.com')}}" crossorigin />
    <link href="{{ asset('https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap')}}" rel="stylesheet" />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{{ asset('../cms/assets/vendor/fonts/boxicons.css')}}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ asset('../cms/assets/vendor/css/core.css')}}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('../cms/assets/vendor/css/theme-default.css')}}" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="{{ asset('../cms/assets/css/demo.css')}}" />
    <link rel="stylesheet" href="{{ asset('../cms/assets/css/pagination.css')}}" />
    <link rel="stylesheet" href="{{ asset('../cms/assets/css/attribute.css')}}" />


    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('../cms/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css')}}" />

    <link rel="stylesheet" href="{{ asset('../cms/assets/vendor/libs/apex-charts/apex-charts.css')}}" />
    <link rel="stylesheet" href="{{ asset('../cms/assets/vendor/css/pages/page-auth.css')}}" />
    <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css') }}">


    <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css') }}" />

    <link href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-rc.0/css/select2.min.css')}}" rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('../cms/assets/css/select2.css')}}" />

    <link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.min.css')}}">

    <!-- Style for add attribute -->
