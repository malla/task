<?php

use App\Http\Controllers\cms\CertificationController;
use App\Http\Controllers\cms\AdminController;
use App\Http\Controllers\cms\AuthController;
use App\Http\Controllers\cms\UserController;
use App\Http\Controllers\cms\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', function () {
    return redirect()->route('cms.auth.login.index');
});


Route::prefix('cms')->name('cms.')->group(function () {

    Route::prefix('auth')->name('auth.')->group(function () {

        Route::prefix('login')->name('login.')->group(function () {
            Route::get('/', [AuthController::class, 'index'])->name('index');
            Route::post('/', [AuthController::class, 'store'])->name('store');
        });

        Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    });

    Route::middleware(['auth', 'active'])->group(function () {
        Route::get('dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');

        Route::prefix('admin')->name('admin.')->group(function () {
            Route::get('/', [AdminController::class, 'index'])->name('index');
            Route::get('add', [AdminController::class, 'add'])->name('add');
            Route::post('/', [AdminController::class, 'store'])->name('store');
            Route::get('edit/{id}', [AdminController::class, 'edit'])->name('edit');
            Route::post('update/{id}', [AdminController::class, 'update'])->name('update');
            Route::get('destroy/{id}', [AdminController::class, 'destroy'])->name('destroy');
            Route::post('status', [AdminController::class, 'status'])->name('status');
        });

        Route::prefix('user')->name('user.')->group(function () {
            Route::get('/', [UserController::class, 'index'])->name('index');
            Route::get('destroy/{id}', [UserController::class, 'destroy'])->name('destroy');
            Route::post('status', [UserController::class, 'status'])->name('status');
        });


        Route::prefix('certification')->name('certification.')->group(function () {
            Route::get('/', [CertificationController::class, 'index'])->name('index');
            Route::get('add', [CertificationController::class, 'add'])->name('add');
            Route::post('/', [CertificationController::class, 'store'])->name('store');
            Route::get('edit/{id}', [CertificationController::class, 'edit'])->name('edit');
            Route::post('update/{id}', [CertificationController::class, 'update'])->name('update');
            Route::get('destroy/{id}', [CertificationController::class, 'destroy'])->name('destroy');
            Route::post('status', [CertificationController::class, 'status'])->name('status');
            Route::get('/exportExcel', [CertificationController::class, 'exportExcel'])->name('exportExcel');
            Route::get('/exportPdf', [CertificationController::class, 'exportPdf'])->name('exportPdf');
        });








    });
});
