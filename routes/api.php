<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\app\AuthController;
use App\Http\Controllers\app\CertificationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::prefix('app')->name('app.')->group(function () {

    // Route::controller(AuthController::class)->group(function () {
    //     Route::post('login', 'login');
    //     Route::post('register', 'register');
    //     Route::post('logout', 'logout');
    //     Route::post('approveUser', 'approveUser');
    //     Route::get('getPendingUsers', 'getPendingUsers');
    //     Route::get('getMyProfile', 'getMyProfile');
    //     Route::post('updateMyProfile', 'updateMyProfile');
    // });

    Route::controller(AuthController::class)->group(function () {
        Route::post('login', 'login');
        Route::post('register', 'register');
        Route::post('logout', 'logout');
        Route::post('approveUser', 'approveUser')->middleware(['checkAdminAndActive']);
        Route::get('getPendingUsers', 'getPendingUsers')->middleware(['checkAdminAndActive']);
        Route::middleware(['checkUserAndActive'])->group(function () {
            Route::get('getMyProfile', 'getMyProfile');
            Route::post('updateMyProfile', 'updateMyProfile');
        });
    });



    // Route::prefix('certifications')->name('certifications.')->group(function () {
    //     Route::get('/', [CertificationController::class, 'index']);
    //     Route::get('show/{id}', [CertificationController::class, 'show']);
    //     Route::post('/store', [CertificationController::class, 'store']);
    //     Route::post('update/{id}', [CertificationController::class, 'update']);
    //     Route::post('delete/{id}', [CertificationController::class, 'destroy']);
    //     Route::get('/getUserCertifications', [CertificationController::class, 'getUserCertifications']);
    //     Route::post('/addUserCertification', [CertificationController::class, 'addUserCertification']);
    //     Route::post('/destoryUserCertification/{id}', [CertificationController::class, 'destoryUserCertification']);
    //     Route::get('/getUserCountByCertification', [CertificationController::class, 'getUserCountByCertification']);

    // });

    Route::prefix('certifications')->name('certifications.')->group(function () {
        Route::get('/', [CertificationController::class, 'index']);
        Route::get('show/{id}', [CertificationController::class, 'show'])->middleware(['checkAdminAndActive']);
        Route::post('/store', [CertificationController::class, 'store'])->middleware(['checkAdminAndActive']);
        Route::post('update/{id}', [CertificationController::class, 'update'])->middleware(['checkAdminAndActive']);
        Route::post('delete/{id}', [CertificationController::class, 'destroy'])->middleware(['checkAdminAndActive']);

        Route::middleware(['checkUserAndActive'])->group(function () {
            Route::get('/getUserCertifications', [CertificationController::class, 'getUserCertifications']);
            Route::post('/addUserCertification', [CertificationController::class, 'addUserCertification']);
            Route::post('/destoryUserCertification/{id}', [CertificationController::class, 'destoryUserCertification']);
        });

        Route::get('/getUserCountByCertification', [CertificationController::class, 'getUserCountByCertification'])->middleware(['checkAdminAndActive']);
    });



});
