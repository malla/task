<?php

namespace App\Exports;

use App\Models\insurance;
use App\Models\insuranceCategory;
use App\Models\Payee;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class CertificationExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
, WithEvents
{
    protected $certifications;protected $totalAmountSum;

    public function headings(): array
    {
        return [
            'Id',
            'Name',
            'Users Count',

        ];
    }

    public function map($insurance): array
    {

        return [
            $insurance->id,
            $insurance->name,
            $insurance->certifications_count,


        ];
    }


    public function __construct($certifications)
    {
        $this->certifications = $certifications;

    }

    public function collection()
    {
        return collect($this->certifications);
    }

    public function registerEvents(): array
    {
        return [
          
        ];
    }
}
