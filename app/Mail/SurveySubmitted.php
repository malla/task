<?php


namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SurveySubmitted extends Mailable
{

    use Queueable, SerializesModels;
    public $mailData;

    public function __construct($mailData)
    {
        $this->mailData = $mailData;

    }

    public function build()
    {

        return $this->view('emails.survey-submitted')
                    ->subject('Survey Submission');
    }
}
