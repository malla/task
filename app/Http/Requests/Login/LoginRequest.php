<?php

namespace App\Http\Requests\Login;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string|min:4',
        ];
    }

    public function messages()
{
    return [
        'email.required' => 'The email field is required.',
        'email.unique' => 'The email address is already in use.',
        'password.required' => 'The password field is required.',
        'password.min' => 'The password must be at least 4 characters long.',

    ];
}
}
