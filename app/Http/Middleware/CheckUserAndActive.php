<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserAndActive
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user && $user->type === 'user' && $user->status === 'active') {
            return $next($request);
        }

        return response()->json(['message' => 'Unauthorized'], 403);
    }
}
