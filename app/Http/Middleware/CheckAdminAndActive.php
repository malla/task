<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdminAndActive
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user && $user->type === 'admin' && $user->status === 'active') {
            return $next($request);
        }

        return response()->json(['message' => 'Unauthorized'], 403);
    }
}
