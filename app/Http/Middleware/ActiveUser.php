<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActiveUser
{
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            $user = Auth::user();
            if (!$user->whereActive()) {
                return redirect()->route('cms.auth.login.index')->with('status', 'Your account is deactivated. Please contact support.');
            }
        }
        return $next($request);
    }
}
