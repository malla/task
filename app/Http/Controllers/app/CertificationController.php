<?php

namespace App\Http\Controllers\app;

use App\Http\Controllers\Controller;
use App\Http\Resources\CertificationResource;
use App\Http\Resources\UserCertificationResource;
use App\Models\Certification;
use App\Models\UserCertification;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
// use Barryvdh\DomPDF\Facade as PDF;
// use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\File;
use Mpdf\Mpdf;

class CertificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' =>
        []]);

        Auth::setDefaultDriver('api');
    }

    public function index()
    {
        $user = auth()->user();
        if ($user->type == 'admin') {
            $certifications = Certification::withCount('certifications')->paginate(10);
        } else {
            $certifications = Certification::where('status', 'active')
                ->whereDoesntHave('certifications', function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                })
                ->withCount('certifications')
                ->paginate(10);
        }

        return response()->json(['success' => true, 'data' => CertificationResource::collection($certifications)], 200);
    }

    public function show($id)
    {
        $user = auth()->user();
        if ($user->type == 'admin') {
            $certification = Certification::withCount('certifications')->where('id', $id)->first();
        } else {
            $userCertification = UserCertification::where('user_id', $user->id)
                ->where('certification_id', $id)
                ->exists();

            if ($userCertification) {
                $certification = Certification::withCount('certifications')->where('id', $id)->first();
            } else {
                $certification = null;
            }
        }
        if (!$certification) {
            return response()->json(['success' => false, 'message' => 'Certification not found'], 200);
        }
        return response()->json(['success' => true, 'data' => new CertificationResource($certification)], 200);
    }

    public function store(Request $request)
    {
        $user = auth()->user();
        if ($user->type != 'admin') {
            return response()->json([
                'success' => false,
                'message' => 'D ont have a permission'
            ], 200);
        }
        $request->validate([
            'name' => 'required|string|max:255|unique:certifications',
        ]);

        $certification = new  Certification();
        $certification->name = $request->name;
        $certification->save();

        return response()->json([
            'success' => true,
            'data' => $certification
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $user = auth()->user();
        if ($user->type != 'admin') {
            return response()->json([
                'success' => false,
                'message' => 'D ont have  a permission'
            ], 200);
        }
        $certification = Certification::find($id);

        if (!$certification) {
            return response()->json(['success' => false, 'message' => 'Certification not found'], 200);
        }

        $request->validate([
            'name' => 'required|string|max:255|unique:certifications,name,' . $id,
            'status' => 'required|in:active,inactive',
        ]);


        $certification->name = $request->name;
        $certification->status = $request->status;
        $certification->save();

        return response()->json(['success' => true, 'data' => $certification], 200);
    }

    public function destroy($id)
    {
        $user = auth()->user();
        if ($user->type != 'admin') {
            return response()->json([
                'success' => false,
                'message' => 'D ont have  a permission'
            ], 200);
        }


        $certification = Certification::find($id);

        if (!$certification) {
            return response()->json(['success' => false, 'message' => 'Certification not found'], 200);
        }

        $exist = UserCertification::where('certification_id', $id)->exists();
        if ($exist) {
            $certification->status = 'inactive';
            $certification->save();

            return response()->json(['success' => true, 'message' => 'Certification inacivated '], 200);
        }

        $certification->delete();
        return response()->json(['success' => true, 'message' => 'Certification deleted successfully'], 200);
    }

    public function getUserCertifications()
    {
        $user = auth()->user();

        $user_certifications = UserCertification::where('user_id', $user->id)->paginate(10);

        return response()->json([
            'success' => true,
            'data' => UserCertificationResource::collection($user_certifications)
        ], 200);
    }

    public function addUserCertification(Request $request)
    {
        $user = auth()->user();
        if ($user->type != 'user') {
            return response()->json([
                'success' => false,
                'message' => 'D ont have  a permission'
            ], 200);
        }
        $request->validate([
            'certification_id' => 'required',

        ]);
        $certification_id = $request->certification_id;
        $certification = Certification::where('id', $certification_id)->exists();

        if (!$certification) {
            return response()->json([
                'success' => false,
                'message' => 'Certification dont found'
            ],200);
        }

        $existingCertification = UserCertification::where('user_id', $user->id)
            ->where('certification_id', $certification_id)
            ->first();

        if ($existingCertification) {
            return response()->json([
                'success' => false,
                'message' => 'User already has this certification'
            ], 200);
        }


        $user_certification = new UserCertification();
        $user_certification->user_id = $user->id;
        $user_certification->certification_id = $certification_id;

        $user_certification->save();

        return response()->json([
            'success' => true,
            'message' => 'Certification added successfully'
        ],200);
    }


    public function destoryUserCertification($id)
    {
        $user = auth()->user();
        if ($user->type != 'user') {
            return response()->json([
                'success' => false,
                'message' => 'D ont have  a permission'
            ],200);
        }
        $userCertification = UserCertification::where('id', $id)->where('user_id', $user->id)->first();
        if (!$userCertification) {
            return response()->json([
                'success' => false,
                'message' => 'D ont found'
            ],200);
        }

        $userCertification->delete();

        return response()->json([
            'success' => true,
            'message' => 'Deleted successfully'
        ],200);
    }

    public function getUserCountByCertification()
    {

        $user = auth()->user();
        if ($user->type != 'admin') {
            return response()->json([
                'success' => false,
                'message' => 'D ont have  a permission'
            ],200);
        }

        $certifications = Certification::withCount('certifications')->get();



        $mpdf = new Mpdf();
        $html = view('pdf.certification', compact('certifications'));
        $mpdf->autoLangToFont = true;
        $mpdf->autoScriptToLang = true;
        $mpdf->WriteHTML($html);


        $mpdf->Output('certification.pdf', \Mpdf\Output\Destination::DOWNLOAD);
    }
}
