<?php

namespace App\Http\Controllers\app;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Exception;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register','getPendingUsers']]);
        auth()->setDefaultDriver('api');
    }




    public function login()
    {
        try {
            request()->validate([
                'email'    => 'required|email',
                'password' => 'required',
            ]);

            $jwtToken = null;

            if (!$jwtToken = auth()->attempt(request()->only('email', 'password'))) {
                return response()->json([
                    'success' => false,
                    'message' => __('Please check your credentials and try again.'),
                    'errors'  => []
                ], 200);
            }

            $user = auth()->user();

            if ($user->status == 'inactive') {

                auth()->guard()->logout();

                return response()->json([
                    'success' => false,
                    'message'    => trans('Your account is deactivated!'),
                    'errors' => [],
                ], 200);
            }


            if ($user->status == 'pending') {

                auth()->guard()->logout();

                return response()->json([
                    'success' => false,
                    'message'    => trans('Your account needs administrator approval!'),
                    'errors' => [],
                ], 200);
            }

            return response()->json([
                'success' => true,
                'token'   => $jwtToken,
                'message' => __('You are logged in successfully.'),
                'data'    => $user,

            ],200);
        } catch (ValidationException $exception) {
            return response()->json([
                'success' => false,
                'message'    => $exception->getMessage(),
                'errors' => $exception->errors(),
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'message'    => $e->getMessage(),
            ], 200);
        }
    }

    public function register(Request $request)
    {

        try {
                $request->validate([
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6',
                    'gender' => 'required|in:male,female',
                    'blood_type' => 'required|in:A+,A-,B+,B-,AB+,AB-,O+,O-',
                ]);

                $user = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'gender' => $request->gender,
                    'blood_type' => $request->blood_type,
                    'status' => 'pending'

                ]);

                // dd($user);
                $user->save();



                $token = Auth::login($user);

                return response()->json([
                    'success' => true,
                    'token'   => $token,
                    'message' => __('You are registered in successfully.'),
                    'data'    =>new UserResource($user),
                ],200);

        } catch (ValidationException $exception) {
            return response()->json([
                'success' => false,
                'message'    => $exception->getMessage(),
                'errors' => $exception->errors(),
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'message'    => $e->getMessage()
            ], 200);
        }
    }



    public function logout()
    {
        auth()->logout();
        return response()->json([
            'success' => true,
            'message' => 'Successfully logged out',
        ],200);
    }


    public function getPendingUsers(){
        $user = auth()->user();
        if($user->type !='admin'){
            return response()->json([
                'success' => false,
                'message' => 'D ont have  a permission'
            ],200);
        }

        $users = User::where('type','user')->where('status','pending')->get();

       return response()->json([
        'success' => true,
        'data' => UserResource::collection($users)
       ],200);
    }

    public function approveUser(Request $request){
        $user = auth()->user();
        if($user->type !='admin'){
            return response()->json([
                'success' => false,
                'message' => 'D ont have  a permission'
            ],200);
        }

        $user_id = $request->user_id;
        if(!$user_id){
            return response()->json([
                'success' => true,
                'message' => 'Pleaser send user_id'
            ],200);
        }

        $user = User::where('id',$user_id)->first();

        if(!$user){
            return response()->json([
                'success' => false,
                'message' => 'User Not Found'
            ],200);
        }


        if($user->type !='user'){
            return response()->json([
                'success' => false,
                'message' => 'this is not a user',
            ],200);
        }

        if($user->status == 'active'){
            return response()->json([
                'success' => false,
                'message' => 'AL ready Activated',
            ],200);
        }

        if($user->status == 'inactive'){
            return response()->json([
                'success' => false,
                'message' => 'this account inactivated',
            ],200);
        }

        if($user->status == 'pending'){
            $user->status = 'active';
            $user->save();
            return response()->json([
                'success' => true,
                'message' => 'this account activated successfully',
            ],200);
        }
    }


    public function getMyProfile(){
        $user = auth()->user();
        if($user->type !='user'){
            return response()->json([
                'success' => false,
                'message' => 'D ont have  a permission'
            ],200);
        }
        $user_id = $user->id;

        $user = User::where('id',$user_id)->where('status','active')->where('type','user')->first();

        return response()->json([
            'success' => true,
            'data' => new UserResource($user)
        ],200);

    }

    public function updateMyProfile(Request $request){

        $user = auth()->user();
        if($user->type !='user'){
            return response()->json([
                'success' => false,
                'message' => 'D ont have  a permission'
            ],200);
        }
        $user_id = $user->id;

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $user_id,
            'gender' => 'required|in:male,female',
            'blood_type' => 'required|in:A+,A-,B+,B-,AB+,AB-,O+,O-',
            'password' => 'nullable|string|min:6',
        ]);

// dd($request->all());
        $user = User::where('id',$user_id)->where('status','active')->where('type','user')->first();
        if(!$user){
            return response()->json([
                'success' =>false,
                'message' => ' account is not active yet'
            ],200);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->blood_type = $request->blood_type;

        if ($request->has('password')) {
            $user->password = Hash::make($request->password);
        }
        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Profile Updated Successfully'
        ],200);
    }

}
