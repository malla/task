<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\Login\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function index(){
        return view('cms.authentication.login');
    }

    public function store(LoginRequest $request)
    {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            if ($user->status === 'inactive') {
                Auth::logout();
                return redirect()->route('cms.auth.login.index')->withError('Your account is deactivated. Please contact support.');
            }

            if ($user->type === 'admin') {
                return redirect()->route('cms.dashboard');
            } elseif ($user->type === 'customer') {
                return redirect()->route('cms.auth.login.index');
            }
        }


        return redirect()->route('cms.auth.login.index')->with('error', 'Invalid credentials! Please try again.');
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('cms.auth.login.index');
    }
}
