<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateAdminRequest;
use App\Http\Requests\Admin\EditAdminRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $status = $request->status;

        $perPage = $request->limit ?? default_limit();

        $admins = User::select('id', 'name', 'email', 'status')
            ->admin()
            ->search($search)
            ->filterByStatus($status)
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);

        return view('cms.admin.index', compact('admins'));
    }

    public function add()
    {
        return view('cms.admin.add');
    }

    public function store(CreateAdminRequest $request)
    {

        $admin = new User();
        $admin->status = $request->active ? 'active' : 'inactive';
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->gender = $request->gender;
        $admin->password = Hash::make($request->password);
        $admin->type = 'admin';

        $admin->save();

        return redirect()->route('cms.admin.index')->with('success', 'Admin created successfully');
    }


    public function edit($id)
    {
        $admin = User::findOrFail($id);
        return view('cms.admin.edit', compact('admin'));
    }


    public function update(EditAdminRequest $request, $id)
    {

        $admin = User::findOrFail($id);


        $admin->status = $request->active ? 'active' : 'inactive';
        $admin->name = $request->name;
        $admin->gender = $request->gender;
        $admin->email = $request->email;
        if ($request->password) {
            $admin->password =  Hash::make($request->password);
        }
        $admin->phone_number = $request->phone_number;
        $admin->save();

        return redirect()->route('cms.admin.index')->with('success', 'Admin updated successfully');
    }


    public function destroy($id)
    {
        $admin = User::findOrFail($id);
        if (!$admin) {
            return redirect()->route('cms.admin.index')->with('error', 'The admin you are trying to delete does not exist.');
        }
        $admin->delete();
        return redirect()->route('cms.admin.index')->with('success', 'Admin deleted successfully');
    }

    public function status(Request $request)
    {
        $admin_id = $request->input('admin_id');
        $status = $request->input('status');

        $admin = User::findOrFail($admin_id);
        $admin->status = $status;
        $admin->save();

        return response()->json([
            'success' => true,
            'message' => 'Status updated successfully'
        ]);
    }
}
