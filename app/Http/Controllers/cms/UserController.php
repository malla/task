<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search ?: null;
        $status = $request->status;

        $perPage = $request->limit ?? default_limit();

        $users = User::select('id', 'name', 'email', 'gender', 'blood_type','status')
        ->user()
        ->search($search)
        ->filterByStatus($status)
        ->orderBy('created_at', 'desc')
        ->paginate($perPage);
        $users->appends(request()->query());

        return view('cms.user.index', compact('users'));
    }
    public function destroy($id)
    {

        $user = User::findOrFail($id);

        $user->delete();

        return redirect()->route('cms.user.index')->with('success', 'User deleted successfully');
    }
    public function status(Request $request)
    {
        $user_id = $request->input('user_id');
        $status = $request->input('status');

        $user = User::findOrFail($user_id);
        $user->status = $status;
        $user->save();

        return response()->json([
            'success' =>true,
            'message' => 'Status updated successfully'
        ]);
    }
}


