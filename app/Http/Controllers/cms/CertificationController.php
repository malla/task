<?php

namespace App\Http\Controllers\cms;

use App\Exports\CertificationExport;
use App\Http\Controllers\Controller;
use App\Models\Certification;
use Illuminate\Http\Request;
use App\Http\Requests\Certification\CreateCertificationRequest;
use App\Http\Requests\Certification\EditCertificationRequest;
use App\Models\UserCertification;
use Mpdf\Mpdf;
use Maatwebsite\Excel\Facades\Excel;

class CertificationController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $status = $request->status;

        $perPage = $request->limit ?? default_limit();

        $certifications = Certification::select('id', 'name', 'status')
        ->withCount('certifications')
            ->search($search)
            ->filterByStatus($status)
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);

        return view('cms.certification.index', compact('certifications'));
    }

    public function add()
    {
        return view('cms.certification.add');
    }

    public function store(CreateCertificationRequest $request)
    {

        $certification = new Certification();
        $certification->status = $request->active ? 'active' : 'inactive';
        $certification->name = $request->name;
        $certification->save();

        return redirect()->route('cms.certification.index')->with('success', 'certification created successfully');
    }


    public function edit($id)
    {
        $certification = Certification::findOrFail($id);
        return view('cms.certification.edit', compact('certification'));
    }


    public function update(EditCertificationRequest $request, $id)
    {

        $certification = Certification::findOrFail($id);


        $certification->status = $request->active ? 'active' : 'inactive';
        $certification->name = $request->name;
        $certification->save();

        return redirect()->route('cms.certification.index')->with('success', 'certification updated successfully');
    }


    public function destroy($id)
    {
        $certification = Certification::findOrFail($id);
        if (!$certification) {
            return redirect()->route('cms.certification.index')->with('error', 'The certification you are trying to delete does not exist.');
        }
        $exist = UserCertification::where('certification_id',$id)->exists();
        if($exist){
            $certification->status = 'inactive';
            $certification->save();

            return redirect()->route('cms.certification.index')->with('error', 'The certification you are trying to delete be inactivated.');
        }
        $certification->delete();
        return redirect()->route('cms.certification.index')->with('success', 'certification deleted successfully');
    }

    public function status(Request $request)
    {
        $certification_id = $request->input('certification_id');
        $status = $request->input('status');

        $certification = Certification::findOrFail($certification_id);
        $certification->status = $status;
        $certification->save();

        return response()->json([
            'success' => true,
            'message' => 'Status updated successfully'
        ]);
    }

    public function exportExcel(Request $request){

        $search = $request->search;
        $status = $request->status;

        $certifications = Certification::select('id', 'name', 'status')
        ->withCount('certifications')
            ->search($search)
            ->filterByStatus($status)
            ->orderBy('certifications_count', 'desc')->get();



            $export = new CertificationExport($certifications);

            return Excel::download($export, 'certifications.xlsx');

    }

    public function exportPdf(Request $request){

        $search = $request->search;
        $status = $request->status;

        $certifications = Certification::select('id', 'name', 'status')
        ->withCount('certifications')
            ->search($search)
            ->filterByStatus($status)
            ->orderBy('created_at', 'desc')->get();



        $mpdf = new Mpdf();
        $html = view('pdf.certification', compact('certifications'));
       $mpdf->autoLangToFont = true;
       $mpdf->autoScriptToLang = true;
       $mpdf->WriteHTML($html);


       $mpdf->Output('certification.pdf', \Mpdf\Output\Destination::DOWNLOAD);

    }
}
