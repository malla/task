<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CertificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $user = auth()->user();

        return [
            'id' => $this->id,
            'store_name' => $this->name,
            'status' => $this->status,
            'user_counts' =>$user->type == 'admin' ? $this->certifications_count ?? 0 : 0,
        ];
    }
}
