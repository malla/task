<?php
function limits($factor = 1)
{
    $limits = [10 * $factor, 25 * $factor, 50 * $factor, 100 * $factor];

    $limitUrls = [];

    foreach ($limits as $limit) {
        $item = [];
        $item['url'] = add_query_params(['limit' => $limit]);
        $item['label'] = $limit;
        $item['active'] = currentLimit() == $limit;
        $limitUrls[] = $item;
    }
    return $limitUrls;
}

function default_limit($factor = 1)
{
    return 10 * $factor;
}

function currentLimit($factor = 1)
{
    if ($limit = request()->get('limit'))
        return  $limit;
    else
        return default_limit($factor);
}

function add_query_params(array $params = [])
{
    $query = array_merge(
        request()->query(),
        $params
    ); // merge the existing query parameters with the ones we want to add

    return url()->current() . '?' . http_build_query($query); // rebuild the URL with the new parameters array
}

?>
