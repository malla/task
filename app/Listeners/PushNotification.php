<?php

namespace App\Listeners;

use App\Http\Resources\OrderResource;
use App\Models\Notification;




class PushNotification
{
    public function afterNotificationAdded($request)
    {
        logger($request);
        logger(1);
        $user_id = 'all';

        $notification = new Notification([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => null,
            'notifiable_type' => 'General',
            'action' => 'GeneralNotification',
            'topic' => $user_id
        ]);

        $notification->notifiable_id = $notification->id;
        $notification->save();

        logger(2);
        $data = [
            'notifiable_type' => 'GeneralNotification',
            'notifiable_id' => $notification->id,
            'notifiable' => $notification,
            'action' => 'GeneralNotification',
            'user_id' => null,
            'title' => $request->title,
            'description' => $request->description,
        ];
        logger(3);
        if ($user_id == 'all') {
            logger(4);
            $this->pushNotificationByTopic(
                $user_id,
                $data['title'],
                $data['description'],
                [
                    'data' => json_encode([
                        'notifiable_type' => $data['notifiable_type'],
                        'notifiable' => $data['notifiable'],
                    ])
                ]
            );
        }
    }

    public function send($data)
    {
        logger(5);
        $this->pushNotification(
            is_array($data['fcm_token']) ? $data['fcm_token'] : [$data['fcm_token']],
            $data['title'],
            $data['description'],
            [
                'data' => json_encode([
                    'id' => $data['id'],
                    'notifiable_type' => $data['notifiable_type'],
                    'notifiable' => $data['notifiable'],
                    'user_id' => isset($data['user_id']) ? $data['user_id'] : null,
                ])
            ]
        );
    }

    public function storeNotification($data)
    {
        logger(6);
        $notification = new Notification();
        $notification->title = $data['title'];
        $notification->description = $data['description'];
        $notification->user_id =  $data['user_id'];
        $notification->notifiable_type = $data['notifiable_type'];
        $notification->notifiable_id =  $data['notifiable_id'];
        $notification->action =  $data['action'];
        $notification->save();
        return $notification->id;
        logger(7);
        logger('Notification Id: ' . $notification->id);
    }


    public function pushNotification($recipients, $title, $body, $payload = [])
    {

        logger(8);
        $serverAuthKey = env('SERVER_AUTH_KEY');

        $headers = [
            'Authorization: key=' . $serverAuthKey,
            'Content-Type: application/json',
            'priority' => 'high',
            'sound' => "default",
            'click_action' => "FLUTTER_NOTIFICATION_CLICK"
        ];

        $data = [
            "registration_ids" => $recipients,
            "notification" => [
                "title" => $title,
                "body" => $body,
                "sound" => "default",
                'priority' => 'high',
            ],
            "data" => array_merge($payload, ['click_action' => "FLUTTER_NOTIFICATION_CLICK"])
        ];
        logger($data['registration_ids']);
        logger($data['notification']);

        $dataString = json_encode($data);

        try {

            $url = 'https://fcm.googleapis.com/fcm/send';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

            $result = curl_exec($ch);

            logger($result);
            curl_close($ch);
            return $result;
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    public function pushNotificationByTopic($topic, $title, $body, $payload = [])
    {

        $serverAuthKey = env('SERVER_AUTH_KEY');

        $headers = [
            'Authorization: key=' . $serverAuthKey,
            'Content-Type: application/json',
            'priority' => 'high',
            'sound' => "default",
            'click_action' => "FLUTTER_NOTIFICATION_CLICK"
        ];

        $data = [
            "to" => "/topics/{$topic}",
            "notification" => [
                "title" => $title,
                "body" => $body,
                "sound" => "default",
                'priority' => 'high',
            ],
            "data" => array_merge($payload, ['click_action' => "FLUTTER_NOTIFICATION_CLICK"])
        ];

        $dataString = json_encode($data);

        try {
            $url = 'https://fcm.googleapis.com/fcm/send';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

            $result = curl_exec($ch);

            logger($result);
            curl_close($ch);
            return $result;
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }
}
